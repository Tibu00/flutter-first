import 'dart:io';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

class AddPage extends StatefulWidget {
  @override
  State<AddPage> createState() => _AddPageState();
}

TextEditingController _latitudeController = TextEditingController();
TextEditingController _longitudeController = TextEditingController();

class _AddPageState extends State<AddPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _titleController = TextEditingController();
  TextEditingController _coordinatesController = TextEditingController();
  TextEditingController _remarkController = TextEditingController();
  DateTime _selectedDate = DateTime.now();
  List<XFile>? _images = [];

  @override
  void initState() {
    super.initState();
    Firebase.initializeApp();
  }

  Future<List<String>> _uploadImages() async {
    List<String> imageUrls = [];
    for (var imageFile in _images!) {
      File file = File(imageFile.path);
      String fileName = 'images/${DateTime.now()}-${imageFile.name}';
      Reference ref = FirebaseStorage.instance.ref().child(fileName);
      await ref.putFile(file);
      String downloadUrl = await ref.getDownloadURL();
      imageUrls.add(downloadUrl);
    }
    return imageUrls;
  }

  void _handleSubmit() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      List<String> imageUrls = await _uploadImages();

      List<String> coords = _coordinatesController.text.split(',');
      double latitude = double.tryParse(coords[0].trim()) ?? 0.0;
      double longitude = double.tryParse(coords[1].trim()) ?? 0.0;
      GeoPoint geoPoint = GeoPoint(latitude, longitude);

      FirebaseFirestore.instance.collection('exploleo').add({
        'title': _titleController.text,
        'coordinates': geoPoint,
        'remark': _remarkController.text,
        'date': _selectedDate,
        'images': imageUrls,
      }).then((_) {
        print("Data added successfully");
        // Handle navigation or clearing form here
      }).catchError((error) {
        print("Failed to add data: $error");
        // Handle errors here
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Pense-bête')),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Form(
            key: _formKey,
            child: ListView(
              children: [
                TextFormField(
                  controller: _titleController,
                  decoration: InputDecoration(labelText: 'Nom de l\'intitulé'),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Veuillez entrer un nom';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: _latitudeController,
                  decoration: InputDecoration(labelText: 'Latitude'),
                  keyboardType: TextInputType.number,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Veuillez entrer une latitude';
                    }
                    if (double.tryParse(value) == null) {
                      return 'Veuillez entrer une latitude valide';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: _longitudeController,
                  decoration: InputDecoration(labelText: 'Longitude'),
                  keyboardType: TextInputType.number,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Veuillez entrer une longitude';
                    }
                    if (double.tryParse(value) == null) {
                      return 'Veuillez entrer une longitude valide';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: _remarkController,
                  decoration:
                      InputDecoration(labelText: 'Remarque sur l\'endroit'),
                ),
                ListTile(
                  title: Text(
                      'Date de localisation: ${DateFormat('dd/MM/yyyy').format(_selectedDate)}'),
                  trailing: Icon(Icons.calendar_today),
                  onTap: () async {
                    final DateTime? picked = await showDatePicker(
                      context: context,
                      initialDate: _selectedDate,
                      firstDate: DateTime(2000),
                      lastDate: DateTime(2025),
                    );
                    if (picked != null && picked != _selectedDate) {
                      setState(() {
                        _selectedDate = picked;
                      });
                    }
                  },
                ),
                IconButton(
                  onPressed: () => _pickImages(),
                  icon: Icon(Icons.photo, semanticLabel: 'Ajouter Photos'),
                ),
                _buildImagePreview(),
                ElevatedButton(
                  onPressed: _handleSubmit,
                  child: Text('Enregistrer'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _pickImages() async {
    final ImagePicker picker = ImagePicker();
    // ignore: unnecessary_nullable_for_final_variable_declarations
    final List<XFile>? pickedFiles = await picker.pickMultiImage();
    if (pickedFiles != null) {
      setState(() {
        _images = pickedFiles;
      });
    }
  }

  Widget _buildImagePreview() {
    if (_images == null || _images!.isEmpty) {
      return Container();
    }
    return Wrap(
      spacing: 8,
      runSpacing: 8,
      children: _images!.map((file) => Image.file(File(file.path))).toList(),
    );
  }
}
