import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_app/pages/add_page.dart';
import 'package:flutter_app/pages/generator_page.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:timezone/standalone.dart';
import '../models/my_app_state.dart';

class MyHomePage extends StatefulWidget {
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var selectedIndex = 0;
  Timer? timer;

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(Duration(seconds: 30), (Timer t) => setState(() {}));
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  String getCurrentDate(String timeZoneId) {
    final timeZone = getLocation(timeZoneId);
    final now = TZDateTime.now(timeZone);
    return DateFormat('EEEE, MMM dd HH:mm', 'fr_FR').format(now);
  }

  @override
  Widget build(BuildContext context) {
    final appState = Provider.of<MyAppState>(context);
    final selectedCountry = appState.selectedCountry;
    final timeZoneId = selectedCountry == SelectedCountry.France
        ? 'Europe/Paris'
        : 'America/Cayenne';

    Widget page;
    switch (selectedIndex) {
      case 0:
        page = GeneratorPage(selectedCountry);
        break;
      case 1:
        page = AddPage();
        break;
      case 2:
        page = Placeholder();
        break;
      default:
        throw UnimplementedError('no widget for $selectedIndex');
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          getCurrentDate(timeZoneId),
          style: TextStyle(fontSize: 18),
        ),
        actions: [
          IconButton(
            icon: selectedCountry == SelectedCountry.France
                ? Image.asset('assets/france_flag.png', width: 30, height: 30)
                : Image.asset('assets/guyane_flag.png', width: 30, height: 30),
            onPressed: () {
              if (selectedCountry == SelectedCountry.France) {
                appState.setCountry(SelectedCountry.Guyana);
              } else {
                appState.setCountry(SelectedCountry.France);
              }
            },
          )
        ],
      ),
      body: LayoutBuilder(
        builder: (context, constraints) {
          if (constraints.maxWidth < 450) {
            return Column(
              children: [
                Expanded(child: page),
                SafeArea(
                  child: BottomNavigationBar(
                    items: [
                      BottomNavigationBarItem(
                        icon: Image.asset(
                          'assets/leopard.png',
                          width: 24,
                          height: 24,
                        ),
                        label: 'Home',
                      ),
                      BottomNavigationBarItem(
                        icon: Icon(Icons.add),
                        label: 'New',
                      ),
                      BottomNavigationBarItem(
                        icon: Icon(Icons.picture_in_picture),
                        label: 'Gallery',
                      ),
                    ],
                    currentIndex: selectedIndex,
                    onTap: (value) {
                      setState(() {
                        selectedIndex = value;
                      });
                    },
                  ),
                )
              ],
            );
          } else {
            return Row(
              children: [
                SafeArea(
                  child: NavigationRail(
                    extended: constraints.maxWidth >= 600,
                    destinations: [
                      NavigationRailDestination(
                        icon: Image.asset(
                          'assets/leopard.png',
                          width: 30,
                          height: 30,
                        ),
                        label: Text('Home'),
                      ),
                      NavigationRailDestination(
                        icon: Icon(Icons.add),
                        label: Text('New'),
                      ),
                    ],
                    selectedIndex: selectedIndex,
                    onDestinationSelected: (value) {
                      setState(() {
                        selectedIndex = value;
                      });
                    },
                  ),
                ),
                Expanded(child: page),
              ],
            );
          }
        },
      ),
    );
  }
}
