import 'package:flutter/material.dart';

enum SelectedCountry {
  // ignore: constant_identifier_names
  France,
  // ignore: constant_identifier_names
  Guyana,
}

class MyAppState extends ChangeNotifier {
  SelectedCountry selectedCountry = SelectedCountry.France;

  void setCountry(SelectedCountry country) {
    selectedCountry = country;
    notifyListeners();
  }
}
